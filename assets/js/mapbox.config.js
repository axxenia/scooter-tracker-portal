/* ------------------------------------------------------------------------------------------------
 * Initialize Mapbox and set default to Singapore coordinates
 * ------------------------------------------------------------------------------------------------ */

mapboxgl.accessToken = 'pk.eyJ1IjoibXlpb3QiLCJhIjoiY2lnbTQ3a2xtMDIzZ3Vha3Iza2t1YWJ1aCJ9.E3Pk7WlTHtZuHi9PCOqtvA';

var map = new mapboxgl.Map({
	container: 'map', // container id
	style: 'mapbox://styles/mapbox/streets-v11',
	center: [103.80, 1.355], // default position
	zoom: 10.5
});


/* ------------------------------------------------------------------------------------------------
 * Function to be called when query button is triggered. 
 * ------------------------------------------------------------------------------------------------ */
function submitSearchRequest() {

	// --- Initialize and reset new maps object
	map = new mapboxgl.Map({
		container: 'map', // container id
		style: 'mapbox://styles/mapbox/streets-v11',
		center: [103.80, 1.355], // default position
		zoom: 10.5
	});

	map.addControl(new mapboxgl.NavigationControl());

	// --- Retrieve all values submitted in the query form
	var numOfScooters = document.getElementById("numOfScooters").value;
	var radius = document.getElementById("radius").value;
	var latitude = document.getElementById("latitude").value;
	var longitude = document.getElementById("longitude").value;


	// --- Create a data object containing the query params
	var data = {numOfScooters, radius, latitude, longitude};

	// --- Initialise an ajax call to the microservice endpoint. 
	// --- On success, we create the markers for the scooters on the map. 
	// --- If the scooter battery is not critical, the marker is in purple. 
	// --- If the scooter battery is in critical condition, the marker will be in red.
	$.ajax ({
		url: 'http://localhost:8080/scooters',
		type: 'POST',
		contentType: "application/json",
		data: JSON.stringify(data),
		dataType: 'json',
		success: function (response) {

			$.each(response, function(index, scooter) {

				if(scooter.status == 'OK') {
					var marker = new mapboxgl.Marker({color:'#7A4AFB'})
							.setLngLat([scooter.longitude, scooter.latitude])
							.addTo(map);
				}
				else {
					var marker = new mapboxgl.Marker({color:'#FA0A20'})
							.setLngLat([scooter.longitude, scooter.latitude])
							.addTo(map);
				}

		    });
		},
		error: function (e) {
			document.getElementById("noticeLabel").innerHTML("An error has been encountered. Please try again.");
		}
	});
}