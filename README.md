# Scooter Search Portal

This is a simple implementation of the scooter search portal built using HTML5 and jQuery. Users may key in the following values to conduct a search :-

- Number of scooters to show
- Radius of the search
- Latitude and longitude values

Mapbox is used to built the map showing the scooters. 



## Running the Service

To run the service, the user simply needs to load `index.html` into a browser. However, the portal has a dependency on the scooter search microservice. Ensure that the service is up and running locally before hitting the 'Submit Query' button in order to view scooters on the map.

